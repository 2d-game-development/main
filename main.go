package main

import (
	"log"
	"tictactoe/game"
)

func main() {
	game := game.CreateTicTacToe()
	err := game.Run()

	if err != nil {
		log.Fatal(err)
	}
}