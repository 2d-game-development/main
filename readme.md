# How to run

**You must have zero engine repo next to this one**

Run the following commands:
1. `go get github.com/hajimehoshi/ebiten/v2`
2. `go build`
3. `./tictactoe.exe ` (windows) or `./tictactoe` (linux)

# How to test

Run `go test ./...` in the root of this repo
