package game

import (
	_ "image/png"

	"zero/utils"
	"zero/scenes"
	"zero/ui"
)

type MenuScene struct {
	scenes.Scene
}

func CreateMenuScene() *scenes.Scene {
	menuScene := &MenuScene{}
	menuScene.InitScene()

	playButton := ui.CreateImageButton("assets/textures/circle.png")
	playButton.SetPosition(utils.Vector2f{X: 100, Y: 100})
	playButton.OnClick.AddCallback(menuScene.play)
	menuScene.AddChild(playButton)

	closeButton := ui.CreateImageButton("assets/textures/cross.png")
	closeButton.SetPosition(utils.Vector2f{X: 100, Y: 300})
	closeButton.OnClick.AddCallback(menuScene.play)
	menuScene.AddChild(closeButton)

	return &menuScene.Scene
}

func (menuScene *MenuScene) play() {
	menuScene.Close(scenes.SceneCloseInfo{
		CloseAction: scenes.PushScene,
		SceneCreator: CreateGameScene,
	})
}

func (menuScene *MenuScene) close() {
	menuScene.Close(scenes.SceneCloseInfo{
		CloseAction: scenes.PopScene,
	})
}