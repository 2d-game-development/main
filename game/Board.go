package game

import (
	"zero/entities"
	"zero/utils"
)

const boardColumns = 3
const boardRows = 3
const boardLinesWidth = 4.0

type Board struct {
	entities.Sprite
	cells [][]*Cell
	OnTurnChange utils.CallbackList
}

func CreateBoard() *Board {
	board := &Board{
		Sprite: *entities.CreateSprite("assets/textures/board.png"),
		cells: [][]*Cell{},
	}
	board.SetOriginCenter()
	board.createCells()

	return board
}

func (board *Board) createCells() {
	for column := 0; column < boardColumns; column++ {
		columnCells := []*Cell{}

		for row := 0; row < boardRows; row++ {
			cell := board.createCell(column, row)
			columnCells = append(columnCells, cell)
			board.AddChild(cell)
		}

		board.cells = append(board.cells, columnCells)
	}
}

func (board *Board) createCell(column int, row int) *Cell {
	cell := CreateCell(board.calculateCellSize(), board.OnTurnChange.Call)
	cell.SetPosition(board.calculatePositionForCell(column, row))

	return cell
}

func (board *Board) calculateCellSize() utils.Vector2f {
	cellFullSize := board.Size.Divide(3.0)
	return cellFullSize.Add(utils.Vector2f{-boardLinesWidth, -boardLinesWidth})
}

func (board *Board) calculatePositionForCell(column int, row int) utils.Vector2f {
	cellSize := board.calculateCellSize()
	position := utils.Vector2f{cellSize.X * float64(column), cellSize.Y * float64(row)}
	offsetLine := boardLinesWidth + 2.0
	offset := utils.Vector2f{offsetLine * float64(column), offsetLine * float64(row)}

	return position.Add(offset)
}

func (board *Board) SetIcon(icon CellIcon){
	for column := 0; column < boardColumns; column++ {
		for row := 0; row < boardRows; row++ {
			board.cells[column][row].SetIcon(icon)
		}
	}
}
