package game

import (
	"zero"
	"zero/utils"
)

var windowSize = utils.Vector2f{X: 400, Y: 600}

func CreateTicTacToe() *zero.Game {
	game := zero.CreateGame(
		zero.GameBuilderParams{
			WindowInfo: utils.WindowInfo{
				Size: utils.Vector2{int(windowSize.X), int(windowSize.Y)},
				Title: "Tic Tac Toe",
				Borderless: true,
			},
			InitialScene: CreateGameScene(),
			BackgroundColor: utils.HexToColor(0xf6f4f4),
		},
	)

	return game
}