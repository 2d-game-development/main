package game

import (
	"zero/entities"
	"zero/ui"
	"zero/utils"
)

type CellIcon string
const (
	NoIcon CellIcon = "NoIcon"
	Cross CellIcon = "cross"
	Circle CellIcon = "circle"
)

type OnSelectedFn = func()

type Cell struct {
	ui.Button
	isSelected bool
	icon CellIcon
	onSelected OnSelectedFn
}

func CreateCell(size utils.Vector2f, onSelected OnSelectedFn) *Cell {
	cell := &Cell{isSelected: false, icon: NoIcon, onSelected: onSelected}
	cell.InitButton()
	cell.Size = size
	cell.OnClick.AddCallback(cell.Select)

	return cell
}

func (cell *Cell) Select() {
	if cell.isSelected {
		return
	}

	cell.isSelected = true
	cell.setRenderIcon(cell.icon)
	cell.onSelected()
}

func (cell *Cell) SetIcon(icon CellIcon){
	cell.icon = icon
}

func (cell *Cell) setRenderIcon(icon CellIcon) {
	textureFileName := "assets/textures/" + string(icon) + ".png"
	sprite := entities.CreateSprite(textureFileName)
	sprite.SetOriginCenter()
	sprite.SetPosition(cell.Size.Multiply(0.5))
	cell.AddChild(sprite)
}