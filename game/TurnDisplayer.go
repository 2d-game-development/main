package game

import (
	"zero/entities"
	"zero/utils"
)

type TurnDisplayer struct {
	entities.BasicEntity
	icon *entities.Sprite
}

func CreateTurnDisplayer(icon CellIcon) *TurnDisplayer{
	turnDisplayer := &TurnDisplayer{}
	turnDisplayer.InitEntity()

	text := entities.CreateText(entities.TextBuilderParams{
		FontFile: "assets/fonts/coolvetica/coolvetica.ttf",
		Text: "TURN",
		FontSize: 30,
		Color: utils.HexToColor(0x5a7d89),
	})
	layout := entities.CreateFlexLayout(entities.FlexLayoutBuilderParams{
		MarginBetween: 10,
	})
	layout.AddChild(text)
	turnDisplayer.AddChild(layout)

	turnDisplayer.icon = entities.CreateEmptySprite()
	turnDisplayer.SetIcon(icon)
	layout.AddChild(turnDisplayer.icon)

	return turnDisplayer
}

func (turnDisplayer *TurnDisplayer) SetIcon(icon CellIcon){
	if	icon == Circle{
		turnDisplayer.icon.SetTexture("assets/textures/circle-icon.png")
	} else {
		turnDisplayer.icon.SetTexture("assets/textures/cross-icon.png")
	}
}