package game

import (
	_ "image/png"
	"zero/scenes"
	"zero/utils"
)

func CreateGameScene() *scenes.Scene {
	gameScene := scenes.CreateScene()
	icon := Cross

	turnDisplayer := CreateTurnDisplayer(icon)
	gameScene.AddChild(turnDisplayer)
	turnDisplayer.SetPosition(utils.Vector2f{X: windowSize.Multiply(0.5).X, Y: 50})

	board := CreateBoard()
	gameScene.AddChild(board)
	board.OnTurnChange.AddCallback(func () {
		if(icon == Cross) {
			icon = Circle
		} else {
			icon = Cross
		}

		board.SetIcon(icon)
		turnDisplayer.SetIcon(icon)
	})
	board.SetPosition(windowSize.Multiply(0.5))
	board.SetIcon(icon)

	return gameScene
}